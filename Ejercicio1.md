using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace contador
{
    class Program
    {
        class CountLetters
        {
            static void Main(string[] args)
            {

                Console.WriteLine("nombre del ficehero: ");
                string nombreFichero = Console.ReadLine();

                Console.WriteLine("Que letras contar");
                string letras = Console.ReadLine();

                StreamReader myFile;
                myFile = File.OpenText(nombreFichero);

                String line;
                int countLetter = 0;
                do
                {
                    line = myFile.ReadLine();
                    if (line != null)
                    {
                        for (int i = 0; i < line.Length; i++)
                        {
                            if (line.Substring(i, 1) == letras)
                            {
                                countLetter++;
                            }
                        }
                    }
                } while (line != null);

                myFile.Close();

                Console.WriteLine("Cantidad de letras: " + countLetter);

                Console.ReadLine();

            }
        }
    }
}